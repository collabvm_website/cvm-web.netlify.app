# Website - [collabvm](http://computernewb.com/collab-vm/) fork
### [My website's](https://ari-web.netlify.app) source code.

credits to [original project](https://github.com/TruncatedDinosour/website)

# Customising when self-hosting
See the `/content/styles/config` and `/content/js/config` directories.
